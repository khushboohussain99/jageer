<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="#">
    <link rel="stylesheet" type="text/css" href="assets/libraries/font-awesome/css/font-awesome.css" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="assets/libraries/jquery-bxslider/jquery.bxslider.css" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="assets/libraries/flexslider/flexslider.css" media="screen, projection">
    <link rel="stylesheet" type="text/css" href="assets/css/realocation.css" media="screen, projection" id="css-main">
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet" type="text/css">
    <title>
        Jageer
    </title>
</head>

<body>
    <!-- /.palette-wrapper -->
    <div id="wrapper">
        <div id="header-wrapper">
            <div id="header">
                <div id="header-inner">
                    <div class="header-bar">
                        <div class="container">
                            <div class="header-infobox">
                                <strong>E-mail:</strong>
                                <a href="#">hello@jageer.com</a>
                            </div>
                            <!-- /.header-infobox-->
                            <div class="header-infobox">
                                <strong>Phone:</strong> 800-123-4567
                            </div>
                            <!-- /.header-infobox-->
                            <ul class="header-bar-nav nav nav-register">
                                <li>
                                    <a href="login">Login</a>
                                </li>
                                <li>
                                    <a href="register">Register</a>
                                </li>
                                <li>
                                    <a href="renew-password">Renew Password</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.container -->
                    </div>
                    <!-- /.header-bar -->
                    <div class="header-top">
                        <div class="container">
                            <div class="header-identity">
                                <a href="/" class="header-identity-target">
                                    <span class="header-icon">
                                        <i class="fa fa-home"></i>
                                    </span>
                                    <span class="header-title">Jageer</span>
                                    <!-- /.header-title -->
                                    <span class="header-slogan">Real Estate &amp; Rental
                                        <br> Bussiness</span>
                                    <!-- /.header-slogan -->
                                </a>
                                <!-- /.header-identity-target-->
                            </div>
                            <!-- /.header-identity -->
                            <div class="header-actions pull-right">
                                <a href="create-agency.html" class="btn btn-regular">Create Agency Profile</a>
                                <strong class="separator">or</strong>
                                <a href="submit-property.html" class="btn btn-primary">
                                    <i class="fa fa-plus"></i>Submit Property</a>
                            </div>
                            <!-- /.header-actions -->
                            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".header-navigation">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- /.container -->
                    </div>
                    <!-- .header-top -->
                    <div class="header-navigation">
                        <div class="container">
                            <div class="row">
                                <ul class="header-nav nav nav-pills">
                                    <li class="menuparent">
                                        <a href="/">Home</a>
                                    </li>
                                    <li class="menuparent">
                                        <a href="#">Properties</a>
                                    </li>
                                    <li class="menuparent">
                                        <a href="#">Agencies</a>
                                    </li>
                                    <li class="menuparent">
                                        <a href="#">Agents</a>
                                    </li>
                                    <li class="menuparent">
                                        <a href="#">Features</a>
                                    </li>
                                    <li class="menuparent">
                                        <a href="#">Blog</a>
                                    </li>
                                </ul>
                                <!-- /.header-nav -->

                            </div>
                        </div>
                        <!-- /.container -->
                    </div>
                    <!-- /.header-navigation -->
                </div>
                <!-- /.header-inner -->
            </div>
            <!-- /#header -->
        </div>

        @yield('content')

          <div id="footer-wrapper">
            <div id="footer">
                <div id="footer-inner">
                    <div class="footer-top">
                        <div class="container">
                            <div class="row">
                                <div class="widget col-sm-8">
                                    <h2>Jageer Features</h2>
                                    <div class="row">
                                        <div class="feature col-xs-12 col-sm-6">
                                            <div class="feature-icon col-xs-2 col-sm-2">
                                                <div class="feature-icon-inner">
                                                    <i class="fa fa-rocket"></i>
                                                </div>
                                                <!-- /.feature-icon-inner -->
                                            </div>
                                            <!-- /.feature-icon -->
                                            <div class="feature-content col-xs-10 col-sm-10">
                                                <h3 class="feature-title">Feature 1</h3>
                                                <p class="feature-body">
                                                    Donec vel tortor eros. Morbi non purus vitae enim semper vehicula.</p>
                                            </div>
                                            <!-- /.feature-content -->
                                        </div>
                                        <!-- /.feature -->
                                        <div class="feature col-xs-12 col-sm-6">
                                            <div class="feature-icon col-xs-2 col-sm-2">
                                                <div class="feature-icon-inner">
                                                    <i class="fa fa-map-marker"></i>
                                                </div>
                                                <!-- /.feature-icon-inner -->
                                            </div>
                                            <!-- /.feature-icon -->
                                            <div class="feature-content col-xs-10 col-sm-10">
                                                <h3 class="feature-title">Feature 2</h3>
                                                <p class="feature-body">
                                                    Donec vel tortor eros. Morbi non purus vitae enim semper vehicula.</p>
                                            </div>
                                            <!-- /.feature-content -->
                                        </div>
                                        <!-- /.feature -->
                                        <div class="feature col-xs-12 col-sm-6">
                                            <div class="feature-icon col-xs-2 col-sm-2">
                                                <div class="feature-icon-inner">
                                                    <i class="fa fa-code"></i>
                                                </div>
                                                <!-- /.feature-icon-inner -->
                                            </div>
                                            <!-- /.feature-icon -->
                                            <div class="feature-content col-xs-10 col-sm-10">
                                                <h3 class="feature-title">Feature 3</h3>
                                                <p class="feature-body">
                                                    Donec vel tortor eros. Morbi non purus vitae enim semper vehicula.</p>
                                            </div>
                                            <!-- /.feature-content -->
                                        </div>
                                        <!-- /.feature -->
                                        <div class="feature col-xs-12 col-sm-6">
                                            <div class="feature-icon col-xs-2 col-sm-2">
                                                <div class="feature-icon-inner">
                                                    <i class="fa fa-flask"></i>
                                                </div>
                                                <!-- /.feature-icon-inner -->
                                            </div>
                                            <!-- /.feature-icon -->
                                            <div class="feature-content col-xs-10 col-sm-10">
                                                <h3 class="feature-title">Feature 4</h3>
                                                <p class="feature-body">
                                                    Donec vel tortor eros. Morbi non purus vitae enim semper vehicula.</p>
                                            </div>
                                            <!-- /.feature-content -->
                                        </div>
                                        <!-- /.feature -->
                                        <div class="feature col-xs-12 col-sm-6">
                                            <div class="feature-icon col-xs-2 col-sm-2">
                                                <div class="feature-icon-inner">
                                                    <i class="fa fa-mobile"></i>
                                                </div>
                                                <!-- /.feature-icon-inner -->
                                            </div>
                                            <!-- /.feature-icon -->
                                            <div class="feature-content col-xs-10 col-sm-10">
                                                <h3 class="feature-title">Feature 5</h3>
                                                <p class="feature-body">
                                                    Donec vel tortor eros. Morbi non purus vitae enim semper vehicula.</p>
                                            </div>
                                            <!-- /.feature-content -->
                                        </div>
                                        <!-- /.feature -->
                                        <div class="feature col-xs-12 col-sm-6">
                                            <div class="feature-icon col-xs-2 col-sm-2">
                                                <div class="feature-icon-inner">
                                                    <i class="fa fa-search"></i>
                                                </div>
                                                <!-- /.feature-icon-inner -->
                                            </div>
                                            <!-- /.feature-icon -->
                                            <div class="feature-content col-xs-10 col-sm-10">
                                                <h3 class="feature-title">Feature 6</h3>
                                                <p class="feature-body">
                                                    Donec vel tortor eros. Morbi non purus vitae enim semper vehicula.</p>
                                            </div>
                                            <!-- /.feature-content -->
                                        </div>
                                        <!-- /.feature -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.widget -->
                                <div class="widget col-sm-4">
                                    <h2>Why Choose Us</h2>
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panel-heading active">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                        Property Management</a>
                                                </h4>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div id="collapseOne" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                            <!-- /.panel-heading -->
                                        </div>
                                        <!-- /.panel -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                        Lifetime Updates</a>
                                                </h4>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div id="collapseTwo" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                            <!-- /.panel-collapse -->
                                        </div>
                                        <!-- /.panel -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                        Free Support</a>
                                                </h4>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div id="collapseThree" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                            <!-- /.panel-collapse -->
                                        </div>
                                        <!-- /.panel -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                                        Rich Documentation</a>
                                                </h4>
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div id="collapseFour" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                                                </div>
                                                <!-- /.panel-body -->
                                            </div>
                                            <!-- /.panel-collapse -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.panel-group -->
                                </div>
                                <!-- /.widget-->
                            </div>
                            <!-- /.row -->
                            <hr>
                            <div class="row">
                                <div class="col-sm-9">
                                    <ul class="footer-nav nav nav-pills">
                                        <li>
                                            <a href="#">Home</a>
                                        </li>
                                        <li>
                                            <a href="#">Properties</a>
                                        </li>
                                        <li>
                                            <a href="#">Agents</a>
                                        </li>
                                        <li>
                                            <a href="#">Agencies</a>
                                        </li>
                                        <li>
                                            <a href="#">Blog</a>
                                        </li>
                                        <li>
                                            <a href="#">Contact</a>
                                        </li>
                                    </ul>
                                    <!-- /.footer-nav -->
                                </div>
                                <div class="col-sm-3">
                                    <form method="post" action="?" class="form-horizontal form-search">
                                        <div class="form-group has-feedback no-margin">
                                            <input type="text" class="form-control" placeholder="Search">
                                            <span class="form-control-feedback">
                                                <i class="fa fa-search"></i>
                                            </span>
                                            <!-- /.form-control-feedback -->
                                        </div>
                                        <!-- /.form-group -->
                                    </form>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container -->
                    </div>
                    <!-- /.footer-top -->
                    <div class="footer-bottom">
                        <div class="container">
                            <p class="center no-margin">
                                &copy; 2018 Jageer, All Right reserved</p>
                            <div class="center">
                                <ul class="social">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-flickr"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-pinterest"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-youtube"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-google-plus"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-vimeo-square"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-dribbble"></i>
                                        </a>
                                    </li>
                                </ul>
                                <!-- /.social -->
                            </div>
                            <!-- /.center -->
                        </div>
                        <!-- /.container -->
                    </div>
                    <!-- /.footer-bottom -->
                </div>
                <!-- /#footer-inner -->
            </div>
            <!-- /#footer -->
        </div>
        <!-- /#footer-wrapper -->
    </div>
    <!-- /#wrapper -->
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/libraries/isotope/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="assets/img/11.jpg"> </script>
    <script type="text/javascript" src="assets/js/gmap3.infobox.js"></script>
    <script type="text/javascript" src="assets/js/gmap3.clusterer.js"></script>
    <script type="text/javascript" src="assets/js/map.js"></script>
    <script type="text/javascript" src="assets/libraries/bootstrap-sass/vendor/assets/javascripts/bootstrap/transition.js"></script>
    <script type="text/javascript" src="assets/libraries/bootstrap-sass/vendor/assets/javascripts/bootstrap/collapse.js"></script>
    <script type="text/javascript" src="assets/libraries/jquery-bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="assets/libraries/flexslider/jquery.flexslider.js"></script>
    <script type="text/javascript" src="assets/js/jquery.chained.min.js"></script>
    <script type="text/javascript" src="assets/js/realocation.js"></script>
</body>

</html>