@extends('base')
@section('content')

 <div id="main-wrapper">
            <div id="main">
                <div id="main-inner">
                    <!-- MAP -->
                    <div class="block-content no-padding">
                        <div class="block-content-inner">
                            <div class="map-wrapper">
                                <div id="map" data-style="1">
                                    <img src="assets/img/11.jpeg" width="100%" height="100%" />
                                    <img src="assets/img/11.jpg" alt="">
                                </div>
                                <!-- /#map -->
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-4 col-sm-offset-8 col-md-3 col-md-offset-9 map-navigation-positioning">
                                            <div class="map-navigation-wrapper">
                                                <div class="map-navigation">
                                                    <form method="post" action="?" class="clearfix">
                                                        <div class="form-group col-sm-12">
                                                            <label>Country</label>
                                                            <div class="select-wrapper">
                                                                <select id="select-country" class="form-control">
                                                                    <option value="pakistan">Pakistan</option>
                                                                </select>
                                                            </div>
                                                            <!-- /.select-wrapper -->
                                                        </div>
                                                        <!-- /.form-group -->
                                                        <div class="form-group col-sm-12">
                                                            <label>Location</label>
                                                            <div class="select-wrapper">
                                                                <select id="select-country" class="form-control">
                                                                    <option value="Lahore">Lahore</option>
                                                                    <option value="Karachi">Karachi</option>
                                                                    <option value="Islamabad">Islamabad</option>
                                                                    <option value="Peshawar">Peshawar</option>
                                                                </select>
                                                            </div>
                                                            <!-- /.select-wrapper -->
                                                        </div>
                                                        <!-- /.form-group -->
                                                        <div class="form-group col-sm-12">
                                                            <label>Property Type</label>
                                                            <div class="select-wrapper">
                                                                <select class="form-control">
                                                                    <option value="apartment">Apartment</option>
                                                                    <option value="building-arae">Building Area</option>
                                                                    <option value="Shops">Shops</option>
                                                                    <option value="house">House</option>
                                                                    <option value="villa">Villa</option>
                                                                </select>
                                                            </div>
                                                            <!-- /.select-wrapper -->
                                                        </div>
                                                        <!-- /.form-group -->
                                                        <div class="form-group col-sm-6">
                                                            <label>Price From</label>
                                                            <input type="text" class="form-control" placeholder="e.g. 1000">
                                                        </div>
                                                        <!-- /.form-group -->
                                                        <div class="form-group col-sm-6">
                                                            <label>Price To</label>
                                                            <input type="text" class="form-control" placeholder="e.g. 5000">
                                                        </div>
                                                        <!-- /.form-group -->
                                                        <div class="form-group col-sm-12">
                                                            <input type="submit" class="btn btn-primary btn-inversed btn-block" value="Filter Properties">
                                                        </div>
                                                        <!-- /.form-group -->
                                                    </form>
                                                </div>
                                                <!-- /.map-navigation -->
                                            </div>
                                            <!-- /.map-navigation-wrapper -->
                                        </div>
                                        <!-- /.col-sm-3 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.container -->
                            </div>
                            <!-- /.map-wrapper -->
                        </div>
                        <!-- /.block-content-inner -->
                    </div>
                    <!-- /.block-content -->
                    <div class="container">
                        <!-- SLOGAN -->
                        <div class="block-content background-primary background-map block-content-small-padding fullwidth">
                            <div class="block-content-inner">
                                <h2 class="no-margin center caps">A good laugh is sunshine in the house.</h2>
                            </div>
                            <!-- /.block-content-iner -->
                        </div>
                        <div class="block-content background-secondary background-map fullwidth" >
                            <h2 class="center">Titanium Agencies</h2>
                            <div class="block-content-inner">
                                <ul class="bxslider clearfix">
                                    <li>

                                        <div class="property-box no-border small">
                                            <div class="property-box-inner">

                                                <!-- /.property-box-label -->
                                                <div class="property-box-picture">
                                                    <!-- /.property-box-price -->
                                                    <div class="property-box-picture-inner">
                                                        <a href="#" class="property-box-picture-target">
                                                            <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                        </a>
                                                        <!-- /.property-box-picture-target -->
                                                    </div>
                                                    <!-- /.property-picture-inner -->
                                                </div>
                                                <!-- /.property-picture -->
                                            </div>
                                            <!-- /.property-box-inner -->
                                        </div>
                                        <!-- /.property-box -->
                                    </li>
                                    <li>

                                        <div class="property-box no-border small">
                                            <div class="property-box-inner">

                                                <!-- /.property-box-label -->
                                                <div class="property-box-picture">
                                                    <!-- /.property-box-price -->
                                                    <div class="property-box-picture-inner">
                                                        <a href="#" class="property-box-picture-target">
                                                            <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                        </a>
                                                        <!-- /.property-box-picture-target -->
                                                    </div>
                                                    <!-- /.property-picture-inner -->
                                                </div>
                                                <!-- /.property-picture -->
                                            </div>
                                            <!-- /.property-box-inner -->
                                        </div>
                                        <!-- /.property-box -->
                                    </li>
                                    <li>

                                        <div class="property-box no-border small">
                                            <div class="property-box-inner">

                                                <!-- /.property-box-label -->
                                                <div class="property-box-picture">
                                                    <!-- /.property-box-price -->
                                                    <div class="property-box-picture-inner">
                                                        <a href="#" class="property-box-picture-target">
                                                            <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                        </a>
                                                        <!-- /.property-box-picture-target -->
                                                    </div>
                                                    <!-- /.property-picture-inner -->
                                                </div>
                                                <!-- /.property-picture -->
                                            </div>
                                            <!-- /.property-box-inner -->
                                        </div>
                                        <!-- /.property-box -->
                                    </li>
                                    <li>

                                        <div class="property-box no-border small">
                                            <div class="property-box-inner">

                                                <!-- /.property-box-label -->
                                                <div class="property-box-picture">
                                                    <!-- /.property-box-price -->
                                                    <div class="property-box-picture-inner">
                                                        <a href="#" class="property-box-picture-target">
                                                            <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                        </a>
                                                        <!-- /.property-box-picture-target -->
                                                    </div>
                                                    <!-- /.property-picture-inner -->
                                                </div>
                                                <!-- /.property-picture -->
                                            </div>
                                            <!-- /.property-box-inner -->
                                        </div>
                                        <!-- /.property-box -->
                                    </li>
                                    <li>

                                        <div class="property-box no-border small">
                                            <div class="property-box-inner">

                                                <!-- /.property-box-label -->
                                                <div class="property-box-picture">
                                                    <!-- /.property-box-price -->
                                                    <div class="property-box-picture-inner">
                                                        <a href="#" class="property-box-picture-target">
                                                            <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                        </a>
                                                        <!-- /.property-box-picture-target -->
                                                    </div>
                                                    <!-- /.property-picture-inner -->
                                                </div>
                                                <!-- /.property-picture -->
                                            </div>
                                            <!-- /.property-box-inner -->
                                        </div>
                                        <!-- /.property-box -->
                                    </li>
                                    <li>

                                        <div class="property-box no-border small">
                                            <div class="property-box-inner">

                                                <!-- /.property-box-label -->
                                                <div class="property-box-picture">
                                                    <!-- /.property-box-price -->
                                                    <div class="property-box-picture-inner">
                                                        <a href="#" class="property-box-picture-target">
                                                            <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                        </a>
                                                        <!-- /.property-box-picture-target -->
                                                    </div>
                                                    <!-- /.property-picture-inner -->
                                                </div>
                                                <!-- /.property-picture -->
                                            </div>
                                            <!-- /.property-box-inner -->
                                        </div>
                                        <!-- /.property-box -->
                                    </li>
                                    <li>

                                        <div class="property-box no-border small">
                                            <div class="property-box-inner">

                                                <!-- /.property-box-label -->
                                                <div class="property-box-picture">
                                                    <!-- /.property-box-price -->
                                                    <div class="property-box-picture-inner">
                                                        <a href="#" class="property-box-picture-target">
                                                            <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                        </a>
                                                        <!-- /.property-box-picture-target -->
                                                    </div>
                                                    <!-- /.property-picture-inner -->
                                                </div>
                                                <!-- /.property-picture -->
                                            </div>
                                            <!-- /.property-box-inner -->
                                        </div>
                                        <!-- /.property-box -->
                                    </li>
                                    <li>

                                        <div class="property-box no-border small">
                                            <div class="property-box-inner">

                                                <!-- /.property-box-label -->
                                                <div class="property-box-picture">
                                                    <!-- /.property-box-price -->
                                                    <div class="property-box-picture-inner">
                                                        <a href="#" class="property-box-picture-target">
                                                            <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                        </a>
                                                        <!-- /.property-box-picture-target -->
                                                    </div>
                                                    <!-- /.property-picture-inner -->
                                                </div>
                                                <!-- /.property-picture -->
                                            </div>
                                            <!-- /.property-box-inner -->
                                        </div>
                                        <!-- /.property-box -->
                                    </li>
                                    <li>

                                        <div class="property-box no-border small">
                                            <div class="property-box-inner">

                                                <!-- /.property-box-label -->
                                                <div class="property-box-picture">
                                                    <!-- /.property-box-price -->
                                                    <div class="property-box-picture-inner">
                                                        <a href="#" class="property-box-picture-target">
                                                            <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                        </a>
                                                        <!-- /.property-box-picture-target -->
                                                    </div>
                                                    <!-- /.property-picture-inner -->
                                                </div>
                                                <!-- /.property-picture -->
                                            </div>
                                            <!-- /.property-box-inner -->
                                        </div>
                                        <!-- /.property-box -->
                                    </li>
                                    <li>

                                        <div class="property-box no-border small">
                                            <div class="property-box-inner">

                                                <!-- /.property-box-label -->
                                                <div class="property-box-picture">
                                                    <!-- /.property-box-price -->
                                                    <div class="property-box-picture-inner">
                                                        <a href="#" class="property-box-picture-target">
                                                            <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                        </a>
                                                        <!-- /.property-box-picture-target -->
                                                    </div>
                                                    <!-- /.property-picture-inner -->
                                                </div>
                                                <!-- /.property-picture -->
                                            </div>
                                            <!-- /.property-box-inner -->
                                        </div>
                                        <!-- /.property-box -->
                                    </li>
                                    <li>

                                        <div class="property-box no-border small">
                                            <div class="property-box-inner">

                                                <!-- /.property-box-label -->
                                                <div class="property-box-picture">
                                                    <!-- /.property-box-price -->
                                                    <div class="property-box-picture-inner">
                                                        <a href="#" class="property-box-picture-target">
                                                            <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                        </a>
                                                        <!-- /.property-box-picture-target -->
                                                    </div>
                                                    <!-- /.property-picture-inner -->
                                                </div>
                                                <!-- /.property-picture -->
                                            </div>
                                            <!-- /.property-box-inner -->
                                        </div>
                                        <!-- /.property-box -->
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- /.block-content-->
                        <!-- ISOTOPE GRID -->
                        <div class="block-content block-content-small-padding">
                            <div class="block-content-inner">
                                <h2 class="center">Featured posts for Sale</h2>
                                <!-- <ul class="properties-filter">
                                        <li class="selected">
                                            <a href="#" data-filter="*"><span>All</span></a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".property-featured"><span>Featured</span></a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".property-rent"><span>Rent</span></a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".property-sale"><span>Sale</span></a>
                                        </li>
                                    </ul> -->
                                <!-- /.property-filter -->
                                <div class="properties-items">
                                    <div class="row">
                                        <div class="property-item property-featured col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Blue Area</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 430 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/8.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>213</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Lahore</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Gulberg</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 1200 / month</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/11.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>292</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Karachi</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Faisal Avenu</a>
                                                    </h4>
                                                    <div class="property-box-label">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/7.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>222</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">F-8</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/4.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>233</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                    </div>
                                    <!-- /.row -->
                                    <div class="row">
                                        <div class="property-item property-featured col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Peshawar</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Ring Road</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 145 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/12.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>203</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Lahore</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Bahria Town</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs.145 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/10.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>273</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">7th Avenue</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 350 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/3.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>297</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Chak Shahzad</a>
                                                    </h4>
                                                    <div class="property-box-label">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs.50 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/6.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>257</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.properties-items -->
                            </div>
                            <div class="block-content-inner" style="margin-top:100px;">
                                <h2 class="center">Featured Propeties for Rent</h2>
                                <!-- <ul class="properties-filter">
                                        <li class="selected">
                                            <a href="#" data-filter="*"><span>All</span></a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".property-featured"><span>Featured</span></a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".property-rent"><span>Rent</span></a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".property-sale"><span>Sale</span></a>
                                        </li>
                                    </ul> -->
                                <!-- /.property-filter -->
                                <div class="properties-items">
                                    <div class="row">
                                        <div class="property-item property-featured col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Blue Area</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 430 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/8.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>213</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Lahore</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Gulberg</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 1200 / month</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/11.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>292</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Karachi</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Faisal Avenu</a>
                                                    </h4>
                                                    <div class="property-box-label">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/7.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>222</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">F-8</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/4.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>233</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                    </div>
                                    <!-- /.row -->
                                    <div class="row">
                                        <div class="property-item property-featured col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Peshawar</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Ring Road</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 145 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/12.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>203</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Lahore</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Bahria Town</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs.145 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/10.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>273</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">7th Avenue</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 350 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/3.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>297</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Chak Shahzad</a>
                                                    </h4>
                                                    <div class="property-box-label">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs.50 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/6.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>257</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.properties-items -->
                            </div>
                           
                            <div class="block-content-inner" style="margin-top:100px;">
                                <h2 class="center">Featured Residential Apartments</h2>
                                <!-- <ul class="properties-filter">
                                        <li class="selected">
                                            <a href="#" data-filter="*"><span>All</span></a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".property-featured"><span>Featured</span></a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".property-rent"><span>Rent</span></a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".property-sale"><span>Sale</span></a>
                                        </li>
                                    </ul> -->
                                <!-- /.property-filter -->
                                <div class="properties-items">
                                    <div class="row">
                                        <div class="property-item property-featured col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Blue Area</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 430 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/8.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>213</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Lahore</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Gulberg</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 1200 / month</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/11.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>292</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Karachi</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Faisal Avenu</a>
                                                    </h4>
                                                    <div class="property-box-label">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/7.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>222</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">F-8</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/4.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>233</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                    </div>
                                    <!-- /.row -->
                                    <div class="row">
                                        <div class="property-item property-featured col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Peshawar</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Ring Road</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 145 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/12.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>203</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Lahore</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Bahria Town</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs.145 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/10.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>273</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">7th Avenue</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 350 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/3.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>297</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Chak Shahzad</a>
                                                    </h4>
                                                    <div class="property-box-label">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs.50 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/6.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>257</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.properties-items -->
                            </div>
                            <div class="block-content-inner" style="margin-top:100px;">
                                <h2 class="center">Featured Housing Projects</h2>
                                <!-- <ul class="properties-filter">
                                        <li class="selected">
                                            <a href="#" data-filter="*"><span>All</span></a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".property-featured"><span>Featured</span></a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".property-rent"><span>Rent</span></a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".property-sale"><span>Sale</span></a>
                                        </li>
                                    </ul> -->
                                <!-- /.property-filter -->
                                <div class="properties-items">
                                    <div class="row">
                                        <div class="property-item property-featured col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Blue Area</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 430 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/8.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>213</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Lahore</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Gulberg</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 1200 / month</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/11.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>292</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Karachi</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Faisal Avenu</a>
                                                    </h4>
                                                    <div class="property-box-label">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/7.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>222</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">F-8</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/4.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>233</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                    </div>
                                    <!-- /.row -->
                                    <div class="row">
                                        <div class="property-item property-featured col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Peshawar</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Ring Road</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 145 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/12.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>203</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Lahore</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Bahria Town</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs.145 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/10.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>273</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">7th Avenue</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 350 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/3.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>297</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Chak Shahzad</a>
                                                    </h4>
                                                    <div class="property-box-label">Sale</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs.50 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/6.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>257</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.properties-items -->
                            </div>
                            <!-- /.block-content-inner -->
                        </div>
                        <div class="block-content block-content-small-padding">
                            <div class="block-content-inner">
                                <h2 class="center">Featured Commercial Projects</h2>
                                <!-- <ul class="properties-filter">
                                            <li class="selected">
                                                <a href="#" data-filter="*"><span>All</span></a>
                                            </li>
                                            <li>
                                                <a href="#" data-filter=".property-featured"><span>Featured</span></a>
                                            </li>
                                            <li>
                                                <a href="#" data-filter=".property-rent"><span>Rent</span></a>
                                            </li>
                                            <li>
                                                <a href="#" data-filter=".property-sale"><span>Sale</span></a>
                                            </li>
                                        </ul> -->
                                <!-- /.property-filter -->
                                <div class="properties-items">
                                    <div class="row">
                                        <div class="property-item property-featured col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Blue Area</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Rent</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 430 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/1.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>213</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Lahore</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Gulberg</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 1200 / month</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/1.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>292</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Karachi</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Faisal Avenu</a>
                                                    </h4>
                                                    <div class="property-box-label">Rent</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/1.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>222</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">F-8</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/1.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>233</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">F-8</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/1.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>233</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">F-8</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/1.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>233</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">F-8</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/1.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>233</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">F-8</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/1.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>233</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">F-8</a>
                                                    </h4>
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 299 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/1.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>233</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                    </div>
                                    <!-- /.row -->
                                    <div class="row">
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Lahore</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Bahria Town</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Rent</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs.145 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/1.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>273</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-rent col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">7th Avenue</a>
                                                    </h4>
                                                    <div class="property-box-label property-box-label-primary">Rent</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs. 350 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/1.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>297</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>3</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                        <div class="property-item property-sale col-sm-6 col-md-3">
                                            <div class="property-box">
                                                <div class="property-box-inner">
                                                    <h3 class="property-box-title">
                                                        <a href="property-detail.html">Islamabad</a>
                                                    </h3>
                                                    <h4 class="property-box-subtitle">
                                                        <a href="property-detail.html">Chak Shahzad</a>
                                                    </h4>
                                                    <div class="property-box-label">Rent</div>
                                                    <!-- /.property-box-label -->
                                                    <div class="property-box-picture">
                                                        <div class="property-box-price">Rs.50 000</div>
                                                        <!-- /.property-box-price -->
                                                        <div class="property-box-picture-inner">
                                                            <a href="property-detail.html" class="property-box-picture-target">
                                                                <img src="assets/img/tmp/properties/medium/1.jpg" alt="">
                                                            </a>
                                                            <!-- /.property-box-picture-target -->
                                                        </div>
                                                        <!-- /.property-picture-inner -->
                                                    </div>
                                                    <!-- /.property-picture -->
                                                    <div class="property-box-meta">
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>1</strong>
                                                            <span>Baths</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Beds</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>257</strong>
                                                            <span>Area</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                        <div class="property-box-meta-item col-xs-3 col-sm-3">
                                                            <strong>2</strong>
                                                            <span>Garages</span>
                                                        </div>
                                                        <!-- /.col-sm-3 -->
                                                    </div>
                                                    <!-- /.property-box-meta -->
                                                </div>
                                                <!-- /.property-box-inner -->
                                            </div>
                                            <!-- /.property-box -->
                                        </div>
                                        <!-- /.property-item -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.properties-items -->
                            </div>
                            <!-- /.block-content-inner -->
                        </div>
                        <!-- /.block-content -->
                        <!-- CAROUSEL -->
                        <div class="block-content background-secondary background-map fullwidth">
                            <h2 class="center">Featured Agents</h2>
                            <div class="block-content-inner">
                                <ul class="bxslider clearfix">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <li>

                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>

                                            <li>

                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                            <li>
                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/4.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <li>

                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>

                                            <li>

                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                            <li>
                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/4.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <li>

                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>

                                            <li>

                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                            <li>
                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/4.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <li>

                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                            <li>
                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/4.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                            <li>
                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/4.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <li>

                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>

                                            <li>

                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                            <li>
                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/4.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <li>

                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>

                                            <li>

                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                            <li>
                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/4.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <li>

                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>

                                            <li>

                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/3.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                            <li>
                                                <div class="property-box no-border small">
                                                    <div class="property-box-inner">

                                                        <!-- /.property-box-label -->
                                                        <div class="property-box-picture">
                                                            <!-- /.property-box-price -->
                                                            <div class="property-box-picture-inner">
                                                                <a href="#" class="property-box-picture-target">
                                                                    <img src="assets/img/tmp/agents/medium/4.jpg" alt="">
                                                                </a>
                                                                <!-- /.property-box-picture-target -->
                                                            </div>
                                                            <!-- /.property-picture-inner -->
                                                        </div>
                                                        <!-- /.property-picture -->
                                                    </div>
                                                    <!-- /.property-box-inner -->
                                                </div>
                                                <!-- /.property-box -->
                                            </li>
                                        </div>
                                    </div>
                                    <li>

                                </ul>
                            </div>
                            <!-- /.block-content-inner -->
                        </div>
                        <!-- /.block-content -->
                        <!-- STATISTICS -->
                        <div class="block-content block-content-small-padding">
                            <div class="block-content-inner">
                                <div class="center">
                                    <h2 class="color-primary">Over 10 000 Properties In Our Directory</h2>
                                </div>
                                <!-- /.center -->
                                <div class="row">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <div class="block-stats background-dots background-primary color-white">
                                            <strong>3500+</strong>
                                            <span>Apartments</span>
                                        </div>
                                        <!-- /.block-stats -->
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="block-stats background-dots background-primary color-white">
                                            <strong>3000+</strong>
                                            <span>Houses</span>
                                        </div>
                                        <!-- /.block-stats -->
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="block-stats background-dots background-primary color-white">
                                            <strong>5000+</strong>
                                            <span>Condos</span>
                                        </div>
                                        <!-- /.block-stats -->
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="block-stats background-dots background-primary color-white">
                                            <strong>2500+</strong>
                                            <span>Areas</span>
                                        </div>
                                        <!-- /.block-stats -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.block-content-inner -->
                        </div>
                        <!-- /.block-content -->
                        <!-- HEXS -->
                        <div class="block-content fullwidth background-primary background-map clearfix">
                            <div class="block-content-inner row">
                                <div class="hex-wrapper col-sm-4 center">
                                    <div class="clearfix">
                                        <div class="hex col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2">
                                            <div class="hex-inner">
                                                <img src="assets/img/hex.png" alt="" class="hex-image">
                                                <div class="hex-content">
                                                    <i class="fa fa-group"></i>
                                                </div>
                                                <!-- /.hex-content -->
                                            </div>
                                            <!-- /.hex-inner -->
                                        </div>
                                        <!-- /.hex -->
                                    </div>
                                    <!-- /.clearfix -->
                                    <h3>15 000+ Satisfied Users</h3>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur diping elit. Curabitur non gravida nisi. Nam vel magna</p>
                                    <a class="btn btn-white" href="#">More</a>
                                </div>
                                <div class="hex-wrapper col-sm-4 center">
                                    <div class="clearfix">
                                        <div class="hex col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2">
                                            <div class="hex-inner">
                                                <img src="assets/img/hex.png" alt="" class="hex-image">
                                                <div class="hex-content">
                                                    <i class="fa fa-search"></i>
                                                </div>
                                                <!-- /.hex-content -->
                                            </div>
                                            <!-- /.hex-inner -->
                                        </div>
                                        <!-- /.hex -->
                                    </div>
                                    <!-- /.clearfix -->
                                    <h3>Smart Property Search</h3>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur diping elit. Curabitur non gravida nisi. Nam vel magna</p>
                                    <a class="btn btn-white" href="#">More</a>
                                </div>
                                <div class="hex-wrapper col-sm-4 center">
                                    <div class="clearfix">
                                        <div class="hex col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2">
                                            <div class="hex-inner">
                                                <img src="assets/img/hex.png" alt="" class="hex-image">
                                                <div class="hex-content">
                                                    <i class="fa fa-compass"></i>
                                                </div>
                                                <!-- /.hex-content -->
                                            </div>
                                            <!-- /.hex-inner -->
                                        </div>
                                        <!-- /.hex -->
                                    </div>
                                    <!-- /.clearfix -->
                                    <h3>We Are Here To Help You</h3>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur diping elit. Curabitur non gravida nisi. Nam vel magna</p>
                                    <a class="btn btn-white" href="#">More</a>
                                </div>
                            </div>
                            <!-- /.block-content-inner -->
                        </div>
                        <!-- /.block-content -->
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /#main-inner -->
            </div>
            <!-- /#main -->
        </div>
        
@endsection